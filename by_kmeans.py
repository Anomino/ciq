# Color sumpling by K-means method.
import numpy as np
import cv2
from sklearn.cluster import KMeans
import time


def get_img_pixels(img):
    height, width = img.shape[:2]
    return np.reshape(img, (height * width, 3))


def sumple_img_by_KMeans(img, K):
    kms = KMeans(n_clusters=K, n_init=1)
    height, width = img.shape[:2]
    copy_img = img.copy()
    pixels = get_img_pixels(img)

    kms.fit(pixels)
    print('clustering finished.')
    for y in range(height):
        for x in range(width):
            _class = kms.predict([img[y, x]])
            replaces_pixel = kms.cluster_centers_[_class].astype(np.int32)
            copy_img[y, x] = replaces_pixel
    return copy_img


def sumple_img_by_MedianCut(img, K):
    """
        Color Image Quantization by Median Cut 

    """
    num_color = 2
    
    src_img = cv2.imread('test.jpg')
    src_vec = src_img.reshape((-1, 3))
    src_vec = np.float32(src_vec)
    height = src_img.shape[0]
    width = src_img.shape[1]
    count_color = height * width
    
    in_vec = np.ones((count_color, 4))
    in_vec[:, :3] = src_vec
    in_vec_idx = np.arange(count_color)

    num_color = 64
    for j in range(num_color - 1):
        print('j')
        print(j)
        num_label = j + 1
        in_class_dif = np.zeros((num_label, 3) ,dtype=np.float32)
        
        if(num_label > 1):
            for i in range(num_label):
                print('in_vec[:,3]!=(i+1)')
                print(in_vec[:, 3] != (i+1))
                in_class = in_vec[in_vec[:, 3] == (i + 1), :3]
                print('in_class.shape')
                print(in_class.shape)
                in_class_max = np.max(in_class, axis=0)
                in_class_min = np.min(in_class, axis=0)
                in_class_dif[i] = in_class_max - in_class_min
            dif_max = np.max(in_class_dif, axis=0)
            max_col = np.max(in_class_dif, axis=1)
            label = np.argmax(max_col) + 1
            comp_num = np.argmax(dif_max)
            print('max_col')
            print(max_col)
            print('label')
            print(label)
        elif(num_label == 1):
            label = 1
            in_class = in_vec[:, :3]
            in_class_max = np.max(in_class, axis=0)
            in_class_min = np.min(in_class, axis=0)
            in_class_dif = in_class_max - in_class_min
            dif_max = np.max(in_class_dif)
            comp_num = np.argmax(in_class_dif)
        else:
            pass
        print('in_class_diff')
        print(in_class_dif)
        print('dif_max')
        print(dif_max)
        print('comp_num')
        print(comp_num)
        in_class = in_vec[in_vec[:,3] == label,:3]
        in_extra = in_vec_idx[in_vec[:,3] == label]
        center_idx = int(np.ceil(in_class.shape[0]/2))
        
        sort_class_idx = np.argsort(in_class, axis=0)
        
        new_label = num_label + 1
        
        in_vec[in_extra[sort_class_idx[(center_idx+1):sort_class_idx.shape[0],comp_num]],3] = new_label

    num_label = np.max(in_vec[:,3])
    class_mean = np.zeros((int(num_label),3),dtype=np.float32)
    for i in range(int(num_label)):
        in_class = in_vec[in_vec[:,3] == i+1,:3]
        in_class_size = in_class.shape[0]
        class_mean[i] = np.mean(in_class,axis=0)
        in_vec[in_vec[:,3] == i+1,:3] = np.matlib.repmat(class_mean[i],in_class_size,1)
        print('i')
        print(i)
        print('in_class')
        print(in_class)
        print('in_class_size')
        print(in_class_size)
        print('class_meam')
        print(class_mean[i])
        print('in_vec[in_vec[:,3] == i+1,:3] = np.matlib.repmat(class_mean[i],in_class_size,1)')
        print(in_vec[in_vec[:,3] == i+1,:3])
    
    out_vec = np.zeros((height*width,3))
    cube_num = np.zeros((height*width,1),dtype=np.uint8)
    tem_vec = np.zeros((num_color,height*width,3),dtype=np.float32)
    dis_vec = np.zeros((height*width,1),dtype=np.float32)
    for i in range(num_color):
        tem_vec[i] = src_vec - class_mean[i]
    tem_vec = tem_vec ** 2
    dis_vec = np.sum(tem_vec,axis = 2)
    dis_vec = np.sqrt(dis_vec)
    dis_vec = dis_vec ** 2
    cube_num = np.argmin(dis_vec,axis=0)
    
    class_mean = np.uint8(class_mean)
    out_vec = class_mean[cube_num.flatten()]
    
    mca_img = out_vec.reshape((src_img.shape))

    return mca_img


def euiclidian_space_dist(x_i, x_j):
    return np.pow(x_i - x_j, 2.0) / len(x_i)


def calc_AveDist(D, d=euiclidian_space_dist):
    dist = 0.0
    n = len(D)
    for x_i in D:
        for x_j in D:
            dist += d(x_i, x_j)

    dist = (2 / (n * (n - 1))) * dist         
    return dist




def calc_density_params_fromD(D, eps, d):
    d_params = np.zeros(len(D))
    for num, x_i in enumerate(D):
        tmp_d = 0.0
        for x_j in D:
            tmp_d += np.sign(eps - d(x_i, x_j))
        d_params[num] = tmp_d     
    return d_params    


def calc_cluster_center(classes, D, cluster_centers):
    pass


def improved_KMeans(D, K, times):
    # Initialize
    dims = len(D[0])
    classes = np.zeros(len(D))
    # Calculate the average distance
    ave_dist = calc_AveDist(D)
    # Calculate the density parameters
    density_params = calc_density_params_fromD(D, eps, d)
    # Sort the density params to select jth largest params
    sorted_dparams = np.sort(density_params)
    centered_v = np.zeros((K, dims))
    for num, param in enumerate(sorted_dparams):
        centered_v[num] = param
    
    for epoch in range(times):     
        for j in range(len(D)):
            # calculate the distance from xj to all the cluster center
            dists = np.zeros(K)
            for num, center in enumerate(centered_v):
                dists[num] = euiclidian_space_dist(D[num], center) 
                
            j_class = np.argmin(dists)    
            classes[j] = j_class

        for j in range(K):
           # caluculate the new cluster center 
           centered_v[j] = calc_cluster_center(D, classes, centered_v)

def make_3d_histgram(img):
    """
        making 3d-histgram from color image

    """
    height, width = img.shape[:2]
    histgram = np.zeros((256, 256, 256), dtype=np.int32) 

    for y in range(height):
        for x in range(width):
            histgram[img[y, x]] += 1 

    return histgram        


def test():
    K = 20  # number of color parrets

    img = cv2.imread('lena.png')
    histgram = make_3d_histgram(img)
    print(histgram)
    current = time.time()
    sumpled_img = sumple_img_by_KMeans(img, K)
    consumed_time = time.time() - current

    cv2.imwrite('{}_sumpled_img.jpg'.format(K), sumpled_img)
    print('finished sumpling {} times'.format(consumed_time))


if __name__ == '__main__':
    test()
